# Journal justification

Before starting a new (fair OA) journal there are several questions that should be considered.

#### Is there a need for a fair OA journal in the respective area?
Justification could come from the fact that there exist no, not enough, or too selective Fair OA journals in this area.
Another reasons could be that a new area has emerged and that, so far, scientific output in this area is scattered in several other journals (and even disciplines) where they don't feel fully at home.
* If you think that there is a need, have you talked with community in the area or is it just your personal thought? 

You may want to have a look at this [list of areas that are in need of fair OA journals](https://gitlab.com/publishing-reform/discussion/blob/master/Founding%20a%20journal/areas_in_need_of_major_foa_journal.md). Note however, that so far (July 2018) the list is not well populated - so if you can, please contribute.

#### Is there a non-fair OA journal that could be flipped?
Whether there exist OA (but not diamond/fair) or non-OA journals that already serve this area, have they been contacted and asked to change their policies? If such a transition can be achieved, the new-old journal will benefit from its current reputation and from the existing infrastructure/in technical matters.

If they are scholarly owned and a full transition to Fair OA is possible, why have they not changed yet? Is only an editor-in-chief blocking, while other editorial members are in favour? Do they maybe just lack the knowledge or time to transition, and if so, have you considered offering your help? Asking the whole editorial board can also help you find supporters for your new journal, in case the old one refuses to flip.

If there exist only publisher-owned journals, a flip to Fair OA usually implies the creation of new journal. However, similar questions as above apply again and maybe the editorial board is willing to move to the new journal. Look for example at [this success story](http://www.mathoa.org/comparing-algebraic-combinatorics-with-its-predecessor/) of Algebraic Combinatorics. For help get in contact with the [Fair Open Access Alliance](https://www.fairopenaccess.org/who-we-are/about-us/) which are already putting effort into flipping journals in linguistics, mathematics, phsychology, and the humanities, and ask in [this forum here](https://gitlab.com/publishing-reform/discussion).

#### Are you up for the task of starting a new journal?
While starting a new journal has become technically easier, it still requires time and effort over a long period. You will also be endowed with responsibilities. So ask yourself honestly...
* whether you can commit the time and effort over a long period,
* whether you are willing to talk with the community (and maybe find and form it first),
* whether you are open to the wishes of the community (e.g. certain type of peer review, etc.),
* whether you are okay with the role of a managing editor and that if you go (hopefully) with elections and term limits for editorial roles, that you might not be chosen as editor-in-chief for life?
A journal should not only be a fun side project, created for your CV, or for someone to have power.