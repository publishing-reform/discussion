# Principles for (new) journals

This document is intended to give a guideline for desirable criteria/principles for journals, whether new or established. 
In general, we take it as given that the [Fair Open Access Principles](https://www.fairopenaccess.org/) (FOAP) are a requirement. However, running/publishing a journal goes beyond what the FOAP consider. 

*This is a document in progress*, help improving and expanding it. See the respective [discussion](https://gitlab.com/publishing-reform/discussion/issues/60). For each aspect, what is the  expected minimum, how can it be done better and best? Why should it be done better? How can a journal or publisher achieve this?

See also the articles [1](https://jscaux.org/blog/post/2017/09/20/noble-metals-noble-cause/) and [2](https://jscaux.org/blog/post/2018/05/05/genuine-open-access/) by [@jscaux](https://gitlab.com/jscaux).

### Community ownership
* **FOAP1**: The Journal has a transparent community-anchored ownership structure, and is controlled by and responsive to the scholarly community.

What does journal ownership mean?:  TODO

* *Minimum*: The controlling organization is not a commercial publisher and must own the journal title. For example, the journal could be run on an informal basis by a team of volunteer academics, like an editorial board. This also mean that a change of service provider can be achieved without changing the title, and so publishing companies simply compete to offer services to the journal.
  * Problems: What if the editor in control decides to take journal to a clossed-access model? What happens when participating parties disagree?
* *Better*: The controlling organization is a fully nonprofit (for example, IRS 501 (c) (3) in the USA, Charitable Incorporated Organisation in the UK) with a clear governance structure. 
  * See for example [Quantum](https://quantum-journal.org/about/) and [Compositionality](http://www.compositionality-journal.org/).

### Accountability to community

### Finances

### Editorial practices

### Peer review

### Licence

The journal should have an OA licence that is stated clearly on every published work and the journal homepage. It is also a FOAP:
* **FOAP3**: All articles are published open access and an explicit open access licence is used.

See **[this file](https://gitlab.com/publishing-reform/discussion/blob/master/Founding%20a%20journal/Licences.md)** for a guide, including an FAQ.
In short a journal should have an OA licence of the following type.

* *Minimum*: Any open access licence, even if proprietary.
* *Good*: A [CC license](https://creativecommons.org/licenses/).
* *Best*: A CC licence [approved for free cultural works](https://creativecommons.org/share-your-work/public-domain/freeworks/), i.e. CC0, CC BY, CC BY-SA.


### Website







