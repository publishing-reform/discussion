# Journal policies

*to be written, help if you can*

Some key areas where an explicitly stated policy is useful to have:

* editorial processes and ethics
* peer review method (single-blind, double-blind, open, number of referees, editorial consensus versus delegation to associate editor, ...)
* publication frequency (continuous versus discrete volumes, monthly versus annual, ...)
* [open access licence](https://gitlab.com/publishing-reform/discussion/blob/master/Founding%20a%20journal/Licences.md), ...
