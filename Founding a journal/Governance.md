# Journal governance

*to be written, help if you can*

The [1. Fair Open Access Principle](https://www.fairopenaccess.org/) says:
> 1. The journal has a transparent ownership structure, and is controlled by and responsive to the scholarly community.

### Ownership


* *Minimum*: The controlling organization is not a commercial publisher and must own the journal title. For example, the journal could be run on an informal basis by a team of volunteer academics, like an editorial board. This also mean that a change of service provider can be achieved without changing the title, and so publishing companies simply compete to offer services to the journal.
  * Problems: What if the editor in control decides to take journal to a closed-access model? What happens when participating parties disagree?
* *Better*: The controlling organization is a fully nonprofit (for example, IRS 501 (c) (3) in the USA, Charitable Incorporated Organisation in the UK) with a clear governance structure. 
  * See for example [Quantum](https://quantum-journal.org/about/) and [Compositionality](http://www.compositionality-journal.org/).

We strongly recommend the second option. Key points are that the controlling organization, not a commercial publisher, must own the journal title (trademarks, domains, ...), so that a change of service provider can be achieved without changing the title. Then publishing companies simply compete to offer services to the journal, rather than exerting control. 


### People

