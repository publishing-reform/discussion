# Open Letter to library consortium negotiators

We, the undersigned, request negotiators worldwide to stand firm and protect interests of all communities involved. We support the following principles for negotiations:

1. We prefer no deal unless these requirements are met.

2. We regard as critical sufficient *transparency* and consultation with all parties represented by the negotiators.
Non-disclosure clauses must not be signed.

3. No multi-year deal involving *lock-in* should be signed. Journals regarded as poor value for money must be allowed to be unsubscribed, with appropriate cost adjustments.

4. Open access payment agreements that cover only selected groups of authors, for example those from a given university or those funded by a given funder, lead to inequitable access for authors to publish, and therefore are inferior to agreements covering publishing for all authors wordwide.

## Notes

1. Recent experiences with cancellations demonstrate that the often-mentioned fear of negative researcher reaction to subscription cancellations is greatly overstated. Such cancellations include:  Germany (Elsevier), Sweden (Elsevier), Taiwan (), Peru (), France (Springer), ... . Major institutions such as University of California are prepared for no deal cancellations backed by wide support from researchers on social media.

2. Not only do such clauses prevent the market from functioning properly and often harm the consortium financially, they are not always legally enforceable. Freedom of information requests (undertaken by researchers at substantial personal cost in many cases) have revealed price information.

3. In particular, traditional multi-year subscription Big Deals should not be signed.

4. Examples of equitable open access funding, covering all authors worldwide, include Open Library of Humanities https://www.openlibhums.org/site/about/the-olh-model/, SciPost https://scipost.org/organizations/ and arXiv https://arxiv.org/help/support/faq.


## Links


* https://www.insidehighered.com/news/2018/12/13/university-california-challenges-elsevier-over-access-scholarly-research
* https://www.insidehighered.com/news/2018/05/08/more-institutions-consider-ending-their-big-deals-publishers
* https://plus.google.com/+PeterSuber/posts/G9wWLUhUomY
* https://sparcopen.org/our-work/big-deal-cancellation-tracking/

