Various terms related to scholarly publishing are not always used consistently. 
Here we give some basic definitions we use.

**Hybrid (OA)** Subscription (toll access) journal with paywall for all articles 
except ones for which the OA fee is paid, by author or sponsor.

**APC** Article Processing Charge. A fee paid to the publisher per article published.

**Fee-based** APC is required to publish. The fee can be paid by 
author or sponsor, at discount or via some kind of deal, or waived for selected 
authors. As long as there are any authors whose fees are not guaranteed to be 
waived up front, the journal is in this category.

**Author-facing charge** APC charged directly to the author. May ultimately be 
paid by sponsor.

**Fair OA**  Satisfies the [Fair Open Access Principles](https://fairopenaccess.org):

1. The journal has a transparent ownership structure, and is controlled by and responsive to the scholarly community.
2. Authors of articles in the journal retain copyright.
3. All articles are published open access and an explicit open access licence is used.
4. Submission and publication is not conditional in any way on the payment of a fee from the author or its employing institution, or on membership of an institution or society.
5. Any fees paid on behalf of the journal to publishers are low, transparent, and in proportion to the work carried out.

**Diamond OA** Satisfying Fair Open Access Principles 3 and 4. 

**Free journal** Satisfying Fair Open Access Principle 1. 
