1) We hereby state our strong opposition to any implementation of
scholarly publishing based on submission and publication fees charged to
authors. This includes page charges for traditional subscription
journals, and open access journals charging an author fee per article (APC).
There must be no financial barriers to submission or publication placed
in the way of authors of research articles. 

2) Our opposition to author fees is based on:

i) the historically observed fact that the global system of research and
scholarship works best when barriers to participation by authors and readers 
are as low as possible; 

ii) the principle of fairness to authors - joining the global
conversation should not be limited by inability to pay, or lack of
powerful sponsors who will pay, but should depend on the quality of the
author's research; 

iii) the fact that author fee waivers that take account of ability to pay are extremely 
difficult to implement, and commonly used waiver schemes introduce unfairness and 
waste of resources;

iv) the clear incentive to publishers using such a
model to maximize revenue by overstating their costs and 
accepting and publishing articles without sufficient selectivity or value being 
added.

3) We do not support the payment/reimbursement of author fees by funding agencies, 
because this does nothing to control the level of such fees, and further privileges 
researchers already helped by funders.

4) Instead, we strongly support the model of open access journals whose
production costs are paid by consortial membership contributions transparently related to publishing costs but 
not directly linked to specific authors or articles (for example Open Library of Humanities, Redalyc, SciPost).  
We call on all research funders to take immediate steps to support such organizations of journals.

5) The above is consistent with and informed by the [4th Fair Open Access Principle](https://fairopenaccess.org).


Signed by: (if you can't edit, email wilson.mark.c@gmail.com to add your name)

* Latindex http://www.latindex.org/latindex/inicio
* Free Journal Network https://freejournals.org
* Redalyc https://www.redalyc.org/
* MSP - Mathematical Sciences Publishers https://msp.org
* Jon Tennant, Open Science MOOC
* Daniel Huerlimann, https://sui-generis.ch

