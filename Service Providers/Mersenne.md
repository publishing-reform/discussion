# [Centre Mersenne](https://www.centre-mersenne.org/)

* Free customized OJS with IT support, DOIs, plagiarism checking
* Other services like typesetting, proofreeding, etc. á la carte
* Centre Mersenne is supported by CNRS and Universite Grenoble-Alpes where it is hosted
* Print on demand

### [Free services](https://www.centre-mersenne.org/services/)
* Hosting, installation, adaptation and maintenance of OJS
* Creation and hosting of a personalised and responsive website for the publication
* Creation of a pesonalised LaTeX layout
* DOI attribution
* Crosslink
* Archiving with CLOCKSS
* Plagiarism detection (limited amount)

### Additional services
* Typesetting
* Proofreading
* Managing editor
* More plagiarism detection
* Print

### Mandatory standards
* Fair OA with CC BY
* Peer review
* Independence and competence of editorial board
* Title ownership
* Journal website complies with COPE's "Principles of Transparency and Best Practice in Scholarly Publishing".

### [Example journals](https://www.centre-mersenne.org/page/the-centre-mersennes-journals_en/) publishing with Centre Mersenne
* [Algebraic Combinatorics](http://algebraic-combinatorics.org/)
* [Annales Henri Lebesgue](https://annales.lebesgue.fr)

The Centre is relatively new but the team behind it has years of experience running
well-known projects such as [Numdam](http://www.numdam.org/) and [Cedram](http://www.cedram.org/?lang=en).
In particular, Cedram has been handling several established journals, such as [Annales de l’institut Fourier](http://aif.cedram.org/?lang=en).