# [Scholastica](https://scholasticahq.com/)
* A modern academic journal management system.
* US$10/submission is the only cost for peer review. 
* Can be used only to organize peer-review (http://quantum-journal.org/ does that), or as a full publishing platform (http://discreteanalysisjournal.com/ does that).
* Extra features such as indexing, DOI assignment, plagiarism checking, etc, must be provided by other means.

[The company](https://scholasticahq.com/about) is based in Chicago.

### [Peer review features](https://scholasticahq.com/features)
* Automatically tracking reviewer deadlines
* Automatically saving all email communication with the relevant manuscript
* Managing versions of files from pre-review to peer review through the production process
* Real-time analytics about time to decision, acceptance rates, and submission volume over time

### [Journal website + OA publishing features](https://scholasticahq.com/publishing-features)
* Beautiful, modern design
* Responsive design for mobile, tablet, desktop, etc.
* Automatic metadata for Google Scholar, etc.
* Analytics (views, downloads, referers, etc.)

### For the journals listed below, Scholastica has provided the following features free of charge
* Help set up their new domain and website design elements
* Migrating over back issues from publisher, including coordinating with old publisher and CrossRef re: DOIs
* Integrate with arxiv.org for submission and publication (both operate as arXiv-overlay journals)
* Planning and consultation calls as needed, e.g. ISSNs, DOIs, references to other services, etc.
* Extra editor training and support
* Coordinating with MathSciNet re: injecting metadata into their index (WIP)

### [Example journals](https://scholasticahq.com/browse-journals) publishing with Scholastica
* http://discreteanalysisjournal.com/
* http://internetmathematicsjournal.com/

### [Pricing packages](https://scholasticahq.com/pricing)
* $10 USD per submission for peer review
* $99 USD per month for website + OA publishing hosting
* HTML+PDF typesetting: $5 per 500 words + $7 per attachment

### More information
* [Homepage with links to individual feature pages] (https://scholasticahq.com/)
* [A short PDF overview of Scholastica] (http://docs.scholastica.s3.amazonaws.com/Scholastica%20overview.pdf)
* [A 1-minute video about Scholastica] (https://www.youtube.com/watch?v=LR68kQmXOD4)
* [Editor Guide with screenshots and videos] (http://help.scholasticahq.com/customer/portal/articles/1228155)
