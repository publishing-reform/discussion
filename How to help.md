#### [UPDATE - Help with our Campaigns](https://gitlab.com/publishing-reform/discussion/-/tree/master/Helping)


# Open Knowledge - how you can help us make a difference

If you share our values that knowledge should not have barriers 
and all participants should have equal opportunities and be fairly rewarded,
please help us make it happen.

## Help to improve this document

Either edit directly or submit a [merge request](https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html) or 
let us know in your comments to [this thread](https://gitlab.com/publishing-reform/discussion/issues/84).

## Most important!

- Inform yourself. Search for reliable sources and learn as much as possible. 
Use [our forum](https://gitlab.com/publishing-reform/discussion/issues) to post or answer questions.
- Do what you can to make or help us make a difference.

## What to look for

#### Paywalls
A scholarly journal may have a paywall for all its articles, or only for some, or only for specific years. 
This makes paywalls harder to recognize, when some articles are available, whereas others are not.
Paywalls can even "move" both ways in time, where article moves from paywall to open access or "backwards".

Every paywall requires expensive software consuming additional bandwidth from user connection,
typically making loading slower and increasing data consumption.
In addition, it is not uncommon for selected papers to be mistakenly paywalled, even if declared Open Access,
regularly posted on the [Paywall Watch Twitter](https://twitter.com/paywallwatch).

Also more refined paywalls exist, where only certain functions such as downloading requires payment.
Typically the article can be viewed online but not saved to hard disk,
which means it cannot be read later without connection or should the connection go down.




#### Excessive prices (subscription or author fees or both)
The reason scholarly journals can charge excessive prices,
that they would not be able to dictate under a more fair competition,
is their unique set of properties, such as scope, quality, approach and readership.
That makes each journal irreplaceable, effectively removing any pricing competition.

Another factor is that journals are selected 
by researchers who are not directly paying subscriptions or fees.
There is, however, less visible hidden costs,
where the same institution or funder have their funds reduced as result of high prices,
leading to diminished funds for the researchers.

Another evidence that the prices demanded can be objectively too high for the [service provided](https://gitlab.com/publishing-reform/discussion/issues/4),
comes from the comparison with many high quality 
[Fair Open Access](https://gitlab.com/publishing-reform/discussion/tree/master/Fair%20Open%20Access) journals,
such as ones in the [Free Journal Network](https://freejournals.org/),
where costs are typically low, yet quality is at least as high as with commercial journals.


#### Author fees
These are any fees charged to authors, for submission, acceptance or any other part of the process.
Sometimes the fees are waved during some periods or for certain authors, or for selected issues.
Often these fees also keep increasing, similar to subscription fees,
thus equally contributing the excessive prices problem.

Author fees have created numerous serious issues, 
such as inequality among authors in their chances to publish, not based on scholarly merit,
or [predatory journals](https://en.wikipedia.org/wiki/Predatory_open-access_publishing),
where a focus on maximizing profit can lead to a pressure to accept more lower quality articles.


Another issue with Author Fees, these are often hidden or hard to find on the journal's website.
Or not included in the emails sent by the journal.
There is generally a higher probabily of quality issues with journals charging author fees,
because they are not subject to the more selective quality control by libraries and institutions.
In addition, they can be used by authors with means or sponsored 
to accomodate otherwise unpublishable or incorrect papers.


#### Is the journal owned by the scholarly community or a commercial entity?
This information is often hidden or even unavailable on journal's webpage, 
yet it is of considerable importance.
A commercial entity, focused on maximizing profit and lowering costs, 
is directly interested in keeping prices high and the service low,
which is enabled by the lack of competition due to 
[journals being irreplaceable](#excessive-prices-subscription-or-author-fees-or-both)

Another negative of aspect of a commercial ownership comes from the power of the owner to make 
[decisions without consultation with academic editors](https://gitlab.com/publishing-reform/discussion/issues/85).
Potential negative consequences include drops in quality of submissions, reluctance to submit by authors, and
disrupting the carefully crafted ecosystem of authors and readers
created in course of years of work.


## Why is Open Knowledge important and how does it affect all of us and our future?

### Affecting society at large

#### Paywalls
Even if you don't read scholarly articles yourself, your life is still largely affected
by open knowledge availability to experts and professionals providing you with services,
of making products you are using or will use. 
Literally all professionals, such as doctors, architects, engineers, environmentalists, politicians,
rely and greatly benefit from verified scientific sourses produced and reviewed by scholars.

Having or lacking access to the relevant knowledge can have far going consequences.
Accessing critical medical research can save lives.
Accessing environmental and geological research at the right time can prevent natural disasters
or allow for better preparation, also potentially resulting in saving lives.
Knowledge insights can allow engineers to prevent catastrophes such as building or bridge collapse.

Important secondary negative effect of putting knowledge behind paywall is
that scholars themselves feel less motivated to make their articles accessible to broader audiences,
beyong the narrow circle of specialists expected to have subscriptions via their institutions.
Consequently, even if the professional pays for the article, it will more likely be harder to understand
and the value of the knowledge to the society will be diminished.


#### Excessive prices


Important secondary negative effect of paying for excessively priced journals,
both for subscription and publishing fees, is the divertion of valuable funds away from scholars.
Meaning less carrier opportunities, less funding, less activities to promote scholarship.




### Affecting scholars
If you are a scholar, you likely look for maximum visibility and impact for your work.
Having your article behind paywall will clearly reduce the number of people who can read it
[with far going negative consequences](https://gitlab.com/publishing-reform/discussion/issues/17),
such as being less discovered, less cited, less invited to seminars and conferences,
and therefore also having reduced career opportunities.
Even if your paper is written only for specialists, [massive worldwide subscription cancellations](https://gitlab.com/publishing-reform/discussion/issues/77)
are on the rise, likely resulting in many of your colleagues also losing access to your publications.

You might be still invited to meetings within your network, but you can miss out on 
building new cross-field collaboration that would be highly beneficial both for your career as scholar
and the society benefitting from your results.
In some particularly bad cases, [entire journals with their content can disapper for years](https://gitlab.com/publishing-reform/discussion/issues/22).

On the other hand, if you pay to publish (or your institution or funder does) in a journal making your paper free to read,
you are still negatively affected, as well as your area and the society at large. 
Any pay-to-publish journal creates the unavoidable inquality, where only authors with means can publish.
Even if your paper is high quality, the paper published next to it might be only there because
the author had the means and could not publish it in a more reputable venue.
At the same time, authors with less means won't send their high quality papers.
Or, if you publish a very high quality paper and help the venue to become more attractive, 
the publisher may decide to raise the publishing fees,
that would decrease the number of quality submissions by authors with less means.
That subsequently will make the venue less regarded and visible, 
potentially reducing the impact also for your paper.

Futhermore, even if you are not paying yourself, your valuable time is consumed by the required paperwork.
The result is less time left for your research, less impact for you and the society.
And since every good paper published increases the attractiveness of the venue,
more researchers will see their time being consumed by more paperwork or efforts spent on getting more funding.
Basically, a chain reaction, where every good contribution can make things worse for everyone.



### Affecting editors
As editor, you are looking to maximize the impact of your editorial work
and keeping up journal's quality standards.

#### Journal Ownership
If your journal is owned by commerical entity, 
you are at risk of future decisions by the management that may affect the journal's quality
and impact of your editorial work over many years.
This includes decisions driven by changes in business environments or arrival of new executives.
A common practice saving costs for the publisher and increasing the dependence of the editor, 
is to [load few editors with more work](https://gitlab.com/publishing-reform/discussion/issues/32),
then compensate buying some teaching load or paying modest salary.



## Help convert existing journals to [FOA](https://gitlab.com/publishing-reform/discussion/tree/master/Fair%20Open%20Access)

## Support [FOA](https://gitlab.com/publishing-reform/discussion/tree/master/Fair%20Open%20Access) journals

## Spread and share your expertise
