# [5 Ways to Be a Better Listener - Shaneé Moret](https://www.linkedin.com/video/live/urn:li:ugcPost:6683756855677726721/)

1. Get rid of distractions.
2. Pay attention to what they are saying.
    1. Hearing is different from listening.
    2. Take down notes.
3. Don’t think about what you want to say next, focus on what they are saying now.
    1. Write what you want to say to avoid thinking about it.
    2. Be present.
4. If you don’t understand something you ask questions until you do.
    1. Have open conversation - how can we help each other stay focused?
    2. If you don't understand something - be honest and say it - I didn't understand it, can you explain a bit more?
5. You don’t interrupt.
    1. Show you are listening - ask the proper questions.
    2. Ask for real life examples.
6. Honesty.
    1. "I don't know the answer, but I'm going to research it today, and get back as soon as I can".
    2. Asking questions to let them know you know what you are talking about.
7. Other.
    1. Not more questions but more quality questions.