# Other Public Discussion Forums around Open Access

## International active forums with publicly available archives from http://oad.simmons.edu/oadwiki/Discussion_forums

- [A2K (Access to Knowledge).](http://lists.keionline.org/mailman/listinfo/a2k_lists.keionline.org) Moderated by Manon Ress of Knowledge Ecology International (KEI)
- [Accès ouvert.](https://groupes.renater.fr/sympa/info/accesouvert) Moderated by Christine Berthaud, Jean-Christophe Peyssard, Jean-François Lutz, Marin Dacos, Pierre Mounier. In french.
- [BOAI Forum.](https://www.budapestopenaccessinitiative.org/forum) The forum associated with the Budapest Open Access Initiative. Moderated by Iryna Kuchma.
- [Global Open Access List (GOAL).](http://mailman.ecs.soton.ac.uk/mailman/listinfo/goal) Moderated by Richard Poynder. This list is the successor to the American Scientist Open Access Forum, which ran from 1998 until 2011, and was moderated by Stevan Harnad.
- [IRtalk.](http://lists.lib.sun.ac.za/mailman/listinfo/irtalk) "To discuss, communicate, share information on all open access and institutional repositories related issues in Africa and South Africa."
