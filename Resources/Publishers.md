Scholarly publishers currently offering Open Access to their articles

# Publishers offering temporary free access during the coronavirus outbreak

- [University of California Press](https://www.ucpress.edu/blog/49700/free-access-to-all-uc-press-journals-through-june-2020/)
- [MIT Press](https://mitpress.mit.edu/blog/articles-understanding-pandemics-and-epidemiology)
- [Ohio State University Press](https://kb.osu.edu/handle/1811/131)
- [Bio One](https://complete.bioone.org/covid-19)
- [Project MUSE](https://about.muse.jhu.edu/resources/freeresourcescovid19/)
- [JSTOR - new public health journals](https://about.jstor.org/l/public-health/)
