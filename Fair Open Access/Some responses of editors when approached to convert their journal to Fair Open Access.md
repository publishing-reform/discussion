These are anonymized.

* Aug 2016

Unfortunately, the situation with [journal] is more complicated; ownership is shared 50%/50% between
Springer and [society].  This makes a departure from Springer
extremely difficult.  The problem is not only how to maintain the
revenue stream for [society] but also that we would need to
compensate Springer for 50% of the value of the journal, plus
possible additional legal problems.

So at this moment I don't see how we could take advantage of your
project.  But please keep me informed about your progress, your
success could catalyze changes even for [journal].

* Sep 2016

Now that I am working at [journal], I realize that the burden on the Editors-In-Chief here is quite heavy. I have no idea how [previous editors] did it alone in the past! Personally, given my high teaching load at [university] and the far-too-many things I’ve agreed to get involved in, I cannot manage to take a responsibility like this without getting course releases — which Elsevier funds. We are also very dependent on the administrative support of Elsevier staff in the day-to-day operations; communicating with editors, experts, referees, authors, etc -- who can be frustratingly unresponsive -- is also a lot of work. 
In principle I would really like to see a proposal like this succeed. As I have mentioned to some of you in the past, I would be very happy to see [journal] lead the way. I would be glad to serve as editor (not editor-in-chief) in a new model like this, but I am afraid that I don't have the time and bandwidth to lead that transition.


* Oct 2016

I believe that for a smaller volume journal focusing more on smaller
research communities this model may work, though I still do not see
longterm reliable archivation solved. For [journal] I do not see this
feasible, yet.

* Oct 2016


The General Editorial Board discussed the issue, but felt that we, as a 
small journal, cannot take a lead in such a change. We should ""sit on 
the fence"" and wait to see the outcome of the move of larger 
organizations. We cannot risk our ongoing good working relations with 
Springer Verlag (which, by the way, does not own the journal, and is 
only the distributor).

* Oct 2016

We deeply appreciate your efforts on behalf of open access publishing. 
We are not ready for this change at this time. However, we would be glad if you kept us informed of further developments concerning your proposal.

* Oct 2016 

We share some of your concerns concerning the accessibility of research papers in the mathematical community and we confirm that the general issue of "open access" is considered quite seriously by several respected members of our community. 
Concerning the real question of your letter, namely if we would consider making a move similar to what happened with Lingua and resign, together with board, from [journal], our answer is negative. 
There are several reasons for that, last but not least the commitment we took with our community and with Springer, an editor which, historically, has been quite helpful to the mathematical community (e.g. with the Lecture Notes series).
We will however continue to follow with great interest the kind of revolution suggested in your letter, which certainly tries to cope with a real problem of our times.


* Oct 2016

Thanks for your message. I discussed it with the other editors in Chief.
Although we agree that it is desirable in principle to transform
the publication system into a free to read one, we believe
that the move proposed is likely to harm the
reputation of the journal, and this together with the fact that the papers
are practically free to read anyway, as there are versions in the arXiv and in homepages of people, convince us that we cannot support this move
at the moment, though we realize this may happen sometime in the future.

* Oct 2016
 
If an author submits a paper to a journal where I have some responsibility, then he/she shall know that I will do my utmost to ensure that the paper will be protected correctly. Among many other things, this means keeping the high standards of the journal. This is particularly important for young researchers seeking a research position, as publications in respected journals are required. Later on, citations in respected journals also play a role. It is not completely clear what "respected" means and it changes over time. Earlier my university used the so-called ISI list. Now it uses a different one. In Denmark we also distinguish between A-journals and B-journals (inspired by the Norwegian list which again was inspired by the Australian list, I believe). The idea is that future allocation of grants will depend on the rankings of the journals you publish in. All this is very sad, but I don’t think it will change for the better.
If a journal is closed down, then what happens then with the impact factor and other classifications, and how does this affect how papers in those journals are classified? What do I tell the people who have entrusted me with their papers? I don’t know.
So, in respect for those who have published in the [journal] in the past, and for those who have submitted papers recently,  I cannot take part in closing down the Journal.

* Nov 2016

I think that it would be fair to say that there are mixed opinions amongst the three managing editors at [journal] as to whether a change in publisher of the kind you propose would be warranted. Thus, I think it would be fair to say that [journal] will not be joining you now.   Speaking for myself, I hope that your initiative is successful and that my fellow editors will come around in due time.  

* Nov 2016 

What matters most to me is that Elsevier offer professional support; I have a journal manager who have years of experience and I have contacts there who are reasonable knowledgable people.  And being old-fashioned, I do like the feel of a paper copy in my hands.  It seems to me your project is based on the idea you only need to set up a website, get submissions, and you have a scientific journal.  We already have this in arXiv; journals do something else.      

* Apr 2017

There are several reasons I would not want to do this.  [journal]
occupies a central place in the discrete mathematics community, well
established, running smoothly, providing a familiar venue.  Elsevier provides
value.  The archives are open, and the model in which academics have electronic
access through MathSciNet funded by universities works.  Furthermore, I believe
that an attempt to take this venue from Elsevier and move it elsewhere under a
different name would be theft.  There would be no compensation, unlike the
compensation journal authors receive in the form of recognition, access to
tenure and salary increases, etc.

Journal publishers have been slow to learn what they owe for access to the work
of the scientific community.  Textbooks publishers are worse, and both are suffering for it.  Elsevier has begun to learn that it must provide
considerable service in return for its profits.  For the present, I see this
as a workable way forward.

* Feb 2018

There are several reasons why I think it would be unwise for me to change publishers:

1) [journal] is both a specialty journal and a diverse journal. I cannot personally handle most of the papers that come to [journal] and I rely heavily on the associate editors. I wouldn’t consider doing this without their input. I think several of them would take the opportunity to unburden themselves of the duty. Because of the diversity of journal interests, it would take some time and effort to reassemble the team.

2) I am compensated, both by Springer (I won’t discuss amounts) and by my institution with some teaching release. I am not sure if [university] would extend that compensation to another publisher, though I can buy out teaching. This time is necessary to be editor-in-chief of the journal.

3) I am not sure that I will still be the editor in 2020. If there is a need to move the journal, the editor at the time should direct it.

4) I am quite pleased with the electronic system that Springer provides. I use it extensively to determine which Associate Editor to choose. I would be lost without the archive of [journal] papers, their dispositions and to whom they were assigned.

* Feb 2018

 
at the moment I'm content with what WdG provide. The journal though not free is not ridiculously expensive.  Please do feel free to ask me again in a years time. 

* Mar 2018

While we both personally, and I imagine most of our editors, are quite positive and  supportive of the idea of reducing subscription prices for mathematical journals and making math research more easily available, we feel rather cautious to take this step now, and to move our journal to a platform we are not really familiar with, which has to prove itself yet. We would thus prefer to wait some time before considering actions of this sort very seriously.

I realize that such an approach possible suffers from over-cautiousness and maybe even from some lack of integrity, but still - we would rather be cautious here at this stage.

At any rate, we certainly wish this enterprise, and you personally, every possible success.

* May 2018

First, within our editorial board (of [journal] ) there has been some discussion of your proposal, but as I noted to you before, there has been some strong opposition --- from the editors; I am not talking about the publisher ---  to the very idea of open access, of any kind.  Indeed, I lost a vote on a motion to ask our handling editors to remind our authors that the publisher already permits them to post their final author-produced post-acceptance manuscripts to the arXiv and to encourage the authors to do so.  In the near to medium term it is unlikely that [journal] will join your efforts.  

Secondly, the social pressure of the kind you envision may have the effect of stimulating a discussion, but I doubt that it will result in radical changes unless it is accompanied by aggressive actions.  Even these may have limited effects; I have in mind the cost of knowledge boycott and the ultimately failed German negotiations with Springer; the French negotiations with Springer and Elsevier seem to be headed towards the same end.  

Finally, in many areas of mathematics, there is room for new high quality journals; this is certainly the case in [subfield], for example.   While you would not obtain the victory of flipping a high cost journal immediately by simply starting a new journal, over time you could out compete the commercial publishers.  This is what MSP has done, though admittedly with a different model from yours.   If you were to set up such a journal or journals, I would be happy to work with you in some way --- for a [subfield] journal, it would have to be as an associate editor; for a journal in general [field], [subfield],  or [subfield], I would be happy to work as a handling or managing editor.  

