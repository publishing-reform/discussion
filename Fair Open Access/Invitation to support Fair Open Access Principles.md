### Help us by supporting [Fair Open Access Principles](https://www.fairopenaccess.org/)

We are asking you to help us support the Fair Open Access Principles on behalf of several organizations:

* Fair Open Access Alliance (https://fairopenaccess.org/) of organizations aimed at facilitating the switch to open access publishing.

* Free Journal Network (http://freejournals.org/steering-committee/) aimed at promoting scholarly journals run according to
the Fair Open Access model (https://www.fairopenaccess.org/).

* The Publishing Reform forum (https://gitlab.com/publishing-reform/discussion/issues) with the goal of opening scholarly publishing to society and
raising standards by making it fairer,
more efficient and productive for researchers.

Our initiative of establishing 
5 key Fair Open Access Principles for academic journals (https://www.fairopenaccess.org/),
which we see as a practical way to avoid both closed access and author-paid open access models,
has attracted considerable
support by renowned researchers (https://gitlab.com/publishing-reform/discussion/blob/master/Fair%20Open%20Access/List%20of%20supporters%20of%20Fair%20Open%20Access.md)

The principles are:

1) The journal has a transparent ownership structure, and is controlled by and responsive to the scholarly community.

2) Authors of articles in the journal retain copyright.

3) All articles are published open access and an explicit open access licence is used.

4) Submission and publication is not conditional in any way on the payment of a fee from the author or its employing institution, or on membership of an institution or society.

5) Any fees paid on behalf of the journal to publishers are low, transparent, and in proportion to the work carried out.

We envision achieving these principles
via a transfer of journal ownership to scholarly organizations,
who will be free to select their publishing service providers on a competitive basis.
Competition between providers will reduce prices well below the current level, which is
dictated by large commercial publishers who currently own the titles.
The much lower costs can be comfortably paid from reallocation of the money currently used for  subscriptions.

The resulting unconditional open access with no author-facing charges 
will open science to society at large and allow efficiency and innovation
currently hampered by paywalled closed access models.

However, it is almost impossible for researchers alone to implement these principles.
We are asking for your help to support these principles publicly.

A simple "Yes" reply to be added the list of supporting organizations will go a long way.
Any further help is of course welcome.
