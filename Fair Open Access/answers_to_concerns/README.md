This directory contains documents giving answers to various concerns expressed by editors about the proposed conversion of their journal to open access. Each file deals with a general topic:

* [Why open access is needed](https://gitlab.com/publishing-reform/discussion/blob/master/Fair%20Open%20Access/answers_to_concerns/Why%20open%20access%20is%20needed.md)
* [Community support](https://gitlab.com/publishing-reform/discussion/blob/master/Fair%20Open%20Access/answers_to_concerns/Community%20support.md)
* [Reputation](https://gitlab.com/publishing-reform/discussion/blob/master/Fair%20Open%20Access/answers_to_concerns/Reputation.md)
* [Logistical issues involving the old journal](https://gitlab.com/publishing-reform/discussion/blob/master/Fair%20Open%20Access/answers_to_concerns/The%20old%20journal.md)
* [Infrastructure](https://gitlab.com/publishing-reform/discussion/blob/master/Fair%20Open%20Access/answers_to_concerns/Infrastructure.md)
* [Transition costs](https://gitlab.com/publishing-reform/discussion/blob/master/Fair%20Open%20Access/answers_to_concerns/Workload.md)
* [Money](https://gitlab.com/publishing-reform/discussion/blob/master/Fair%20Open%20Access/answers_to_concerns/Money.md)
