It is reasonable to be concerned about the workload involved in a transition of a journal to Fair Open Access. MathOA was formed to simplify the process
and reduce the work required by editors. 

There are some one-off tasks that need to be done when setting up a new journal title. 

* Indexing is relatively straightforward. 
The most difficult is getting listed in databases such as Scopus and Thomson Reuters. However for an existing journal that is merely changing publisher (albeit with a possible name change) this is easier than for a completely new journal. Even in the latter case, as long as the journal is publishing regularly it is usually just a matter of filling in a form and waiting several months. In some cases the publisher will assist with this, and in the other cases, MathOA will help. 

* Another task is to decide on service providers and test the editorial software. MathOA has a list of recommended service providers and can help to arrange demonstrations and discussions. Modern providers who support the Fair OA model are much more focused on helping editors than the large commercial publishers.



The next concern is about long-term workload. Although purely volunteer-run journals can be very successful, many editors are not ready for that level of commitment. 

* Modern efficient journal software takes away much of the workload.
* DOI assignment can be annoying. Mersenne, for example, does that for journals. Scholastica does not, but Discrete Analysis does.
* It is highly recommended that a model in which the workload is distributed over the whole editorial board, and not just on a small number of editors, be adopted.

