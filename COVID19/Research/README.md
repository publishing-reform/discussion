Community resources on finding and accessing research articles - all contributions are welcome!

**Free copies of some paywalled articles can be found with [UnPaywall](http://unpaywall.org/)**

In the event of any article access difficulties, please [open an issue](https://gitlab.com/publishing-reform/discussion/-/issues/new) 
to recieve help


# Search Engines

## [Dimensions](https://www.dimensions.ai/)

Dimensions is dynamic linked research data platform that re-imagines the way research can be discovered, accessed and analyzed.
Uses AI to analyse its data, indexes publications, covers clinical trials and datasets in addition to the traditional publications.
Provides an Open Access filter via [UnPaywall](http://unpaywall.org/) and operates a freemium access model to its data.
Stats as of 18 March 2020: publications: 2956, trials: 570, datasets: 23.
Part of [Digital Science](https://www.digital-science.com/).

1. [COVID19 related research articles published in 2020.](https://covid-19.dimensions.ai) 3,880 publications as of 22 March 2020, including:
1. [Open Access only articles from the list 1.](https://app.dimensions.ai/discover/publication?search_text=%222019-nCoV%22%20OR%20%22COVID-19%22%20OR%20%E2%80%9CSARS-CoV-2%E2%80%9D%20OR%20%22HCoV-2019%22%20OR%20%22hcov%22%20OR%20%22NCOVID-19%22%20OR%20%20%22severe%20acute%20respiratory%20syndrome%20coronavirus%202%22%20OR%20%22severe%20acute%20respiratory%20syndrome%20corona%20virus%202%22%20OR%20((%22coronavirus%22%20%20OR%20%22corona%20virus%22)%20AND%20(Wuhan%20OR%20China%20OR%20novel))&search_type=kws&search_field=full_search&or_facet_open_access_status_free=oa_all)
  2,382 publications as of 22 March 2020 
1. [Articles from the list 2. sorted by citations.](https://app.dimensions.ai/discover/publication?search_text=%222019-nCoV%22%20OR%20%22COVID-19%22%20OR%20%E2%80%9CSARS-CoV-2%E2%80%9D%20OR%20%22HCoV-2019%22%20OR%20%22hcov%22%20OR%20%22NCOVID-19%22%20OR%20%20%22severe%20acute%20respiratory%20syndrome%20coronavirus%202%22%20OR%20%22severe%20acute%20respiratory%20syndrome%20corona%20virus%202%22%20OR%20((%22coronavirus%22%20%20OR%20%22corona%20virus%22)%20AND%20(Wuhan%20OR%20China%20OR%20novel))&search_type=kws&search_field=full_search&and_facet_year=2020&or_facet_open_access_status_free=oa_all&order=times_cited)


## World Health Organization (WHO)

- [Global research on coronavirus disease (COVID-19)](https://www.who.int/emergencies/diseases/novel-coronavirus-2019/global-research-on-novel-coronavirus-2019-ncov)

# [Journal collections from Coronavirus Tech Handbook](https://docs.google.com/document/d/111k1L5D9TZNShV5Gr2AJ7grfuXmEx_dYXAZcNwEebQI/edit#)
- [The New England Journal of Medicine](https://buff.ly/33ptfCI)
- [The Lancet](https://bit.ly/2xi6Rz1)
- [JAMA Network](https://bit.ly/3adWBXu)
- [The BMJ](https://bit.ly/2U59a1y)
- [Annals of Internal Medicine](https://bit.ly/2U9wmfa)
- [Dynamed](https://bit.ly/396oVcU)



# Publishers offering Open Access

- [List of publishers offering free access to their content during the coronavirus outbreak](https://gitlab.com/publishing-reform/discussion/-/blob/master/Resources/Publishers.md)
- [Vendor COVID-19 Related Donations and Pro Bono Access](https://tinyurl.com/vendorsupportedaccess)


# Datasets

- [COVID-19 Open Research Dataset (CORD-19)](https://pages.semanticscholar.org/coronavirus-research)
