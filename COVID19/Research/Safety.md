# Research on COVID19 Safety

Selected sources with emphasis on peer-reviewed articles

All contributions are welcome!

1. Please mark any article that is NOT peer-reviewed.
1. For any article without open access please also link to its free version if possilbe (use https://unpaywall.org/ to find the free versions).

#### Ng K, Poon BH, Kiat Puar TH, et al. COVID-19 and the Risk to Health Care Workers: A Case Report. Ann Intern Med. 2020; [Epub ahead of print 16 March 2020]. doi: https://doi.org/10.7326/L20-0175