#### IMPORTANT - please [sign to comment and receive updates by email](https://gitlab.com/publishing-reform/discussion/-/issues/134)

# Help with our [Campaigns for the Common Good](https://gitlab.com/publishing-reform/discussion/-/issues/133)!

## We need your help:
- **Brainstorming help**: Share your ideas, to make our campaigns more effective and useful.
- **Content help**: Help improve our messages by leaving comments in [discussion threads](https://gitlab.com/publishing-reform/discussion/-/issues).
- **Community help**: Share with your communities, friends, followers on social media and invite them to help us.
- **Management help**: Help us with organization, onboarding and communication. 
- **Technical help**: Websites, servers, mailing lists, productivity tools, connecting to other services.
- **Funding help**: We are unpaid volunteers dedicating our time to the cause. Some of us don't have present jobs, and we would like to compensate them for their contribution.


## Ongoing Campaigns:
- [Open Letter - Our Science is in Danger and we need Your Help!!!](https://gitlab.com/publishing-reform/discussion/-/issues/133)


## Contact:
- We encourage open public discussion in comments for each discussion, for maximum opennes and transparency. 
- **You can edit or delete your comments at any time**.
- If you prefer email contact, please write to me at dmitri14@gmail.com
