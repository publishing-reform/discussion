# Jon Tennant

## Videos

- [Jon Tennant: "Open Science is just good science" (DARIAH Annual Event 2018)](https://www.youtube.com/watch?v=UEEcwRUgQu8)
- [Jon Tennant (with Spanish translation) - Hatun Tinkuy Perú - Colaboración Entre Pares del Concytec](https://www.youtube.com/watch?v=QvnkcH6KnvU&feature=youtu.be&t=2910)
- [Jon Tennant: open access, disruption of the publishing industry & Latin America #HatunTinkuyPerú](https://www.youtube.com/watch?v=wtPuf6zDiqw)